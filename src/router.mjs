import express from 'express';
import Counter from './counter.mjs';
import logger from './logger.mjs';
import endpoint_mw from './endpoint.mw.mjs';

const app = express();
const counter = new Counter();

// Middleware
app.use(endpoint_mw);

// Endpoints
app.get('', (req, res) => {
    logger.http("[ENDPOINT] GET '/'")
    res.send("Welcome!");
});
// GET /counter-increase
app.get('/counter-increase', (req, res) => {
    logger.http("[ENDPOINT] GET '/counter-increase'")
    counter.increase();
    logger.info(`[COUNTER] increase ${counter.read()}`)
    res.send(`${counter.read()}`);
});

// GET /counter-read
app.get('/counter-read', (req, res) => {
    logger.http("[ENDPOINT] GET '/counter-read'")
    logger.info(`[COUNTER] read ${counter.read()}`)
    //const url = req.url; // mikä on?
    //const method = req.method // voiko hyödyntää
    res.send(`${counter.read()}`);
});

// GET /counter-zero
app.get('/counter-zero', (req, res) => {
    logger.http("[ENDPOINT] GET '/counter-zero'")
    counter.zero();
    logger.info(`[COUNTER] zeroed ${counter.read()}`)
    res.send(`${counter.read()}`);
});

app.get('/non-existing', (req, res) => {
    logger.http("[ENDPOINT] GET '/non-existing'")
    res.status(404).send("Resourse not found.");
});

app.all('*', (req, res) => {
    logger.http("[ENDPOINT] POST '/non-existing'")
    res.status(404).send("Resourse not found.");
});

export default app;
