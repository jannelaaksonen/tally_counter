/** @type {import('express').RequestHandler;} */
const endpoint_mw = (req, res, next) => {
    console.log(`Request received: ${req.method} ${req.url}`);
    next();
};

export default endpoint_mw;
