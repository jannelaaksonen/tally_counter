class Counter {
    constructor(initialValue = 0) {
        this.count = initialValue;
    }

    increase() {
        this.count++;
    }

    zero() {
        this.count = 0;
    }

    read() {
        return this.count;
    }
    
    
}

export default Counter;
